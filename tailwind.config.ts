import { nextui } from "@nextui-org/react"

module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
    "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}"
  ],
  theme: {
    extend: {
      fontSize: {
        large: "2.5rem", // text-large - 45px
        paragraph: '1.333rem', //text-paragraph - 24px
        medium: '1.099rem', // text-medium - 20px
        normal: '1rem', //text-normal - 18px
        small: '0.778rem' //text-small - 14px
      },
      fontFamily: {
        'poppins': ['Poppins', 'sans-serif'],
      },
      lineHeight: {
        normal: "2.222rem", // text-large - 40px
        medium: '1.099rem' // medium - 20px
      },
      boxShadow: {
        'normal': '0 6 20px rgba(0, 0, 0, 0.1)',
      },
      keyframes: {
        bounce: {
          '0%, 100%': { transform: 'translateY(-5%)' },
          '50%': { transform: 'translateY(0)' },
        },
      },
      backgroundImage: {
        'hero-banner': "url('/assets/images/header-banner.png')",
        'footer-texture': "url('/assets/images/footer.svg')",
        'contact-img': "url('/assets/images/contact-bg.png')",
        'about-banner': "url('/assets/images/about_us_banner.png')",
        'price-bg': "url('/assets/images/price_bg.png')",
        'video-bg': "url('/assets/images/video-bg.png')",
      }


    }
  },
  plugins: [nextui({
    themes: {
      light: {
        extend: "light", // <- inherit default values from light theme
        colors: {
          background: "#FEFDFD", // white
          content1: '#111010', // black
          content2: '#FCFFFBCC', // white -80%
          content3:'#BA6CFF', //secondary
          primary: {
            50: "#F6A037",
            100: "#0346B526", // blue - 15% 
            500: "#5E46C9",  // primary - blue
            DEFAULT: "#5E46C9",
            foreground: "#ffffff",
          },
          secondary: {
            50: '#667273',
            100: 'hsla(100, 10%, 94%, 1)',
            300: '#F0F2EF',
            DEFAULT: '#1DA8E7'
          },
          focus: "#F6A037",
        },
        layout: {
          disabledOpacity: "0.3",
          fontSize: {
            tiny: "0.75rem", // text-tiny
            small: "0.875rem", // text-small
            medium: "1rem", // text-medium
            // large: "2.5rem", // text-large
          },
         radius:{
          medium:'10px'
         }
        },
      },
    }
  })],
}
