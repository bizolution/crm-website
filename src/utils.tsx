export function numberWithCommas(num:Number, isRs = null) {
    let number = Number(num)
    if (!isRs) {
      return new Intl.NumberFormat('en-IN', { style: 'currency', currency: "INR" }).format(number).replace(/^(\D+)/, '$1 ');
    }
    else {
      return new Intl.NumberFormat('en-IN', { minimumFractionDigits: 2, }).format(number);
    }
  }
  

export  function add3Dots(string:any, limit:any)
  {
    var dots = "...";
    if(string?.length > limit)
    {
      // you can also use substr instead of substring
      string = string.substring(0,limit) + dots;
    }
  
      return string;
  }
  