"use client"
import { Card, Image, Textarea } from '@nextui-org/react'
// import toast from 'react-hot-toast';
import React from 'react'
import { BiPhoneCall, BiEnvelopeOpen, BiMap } from 'react-icons/bi';
import { Controller, useForm } from "react-hook-form";
// import { Button, InputField, Typography } from '@/components/base'

import { Button, InputField, Typography } from '@/Components/Base';
import Link from 'next/link';

export const Contact = () => {
    const {
        handleSubmit,
        control,


        formState: { errors },
    } = useForm({
        // resolver: zodResolver(loginSchema),
        mode: "onChange",
        defaultValues: {
            first_name: '',
            last_name: '',
            mobile: '',
            email: '',
            message: ''
        }
    });

    async function onSubmit(data: any) {

        let newData = {
            phone: data?.mobile,
            message: data?.message,
            email: data?.email,
            fullname: data.first_name + ' ' + data?.last_name
        }
       

    }


    return (
        <Card className='rounded-md bg-white mb-[8.888rem] lg:-mt-56 -mt-96' id="contact">
            <div className="flex max-h-[613px]">

                {/* INFORMARTION */}
                <div className='bg-contact-img w-[581px] bg-cover bg-no-repeat m-0 hidden pt-[60px] pl-[84px] pr-[56px] pb-28  bg-black lg:block' 
                // style={{ backgroundImage:' lightgray -54.544px -158.633px'}}
                >
                    <div className="text-background mb-20">
                        <div className='w-[346px]'>

                            <Typography variant='h1' className={' font-medium text-large   '}>
                                Let’s Talk
                            </Typography>
                            <Typography variant='p' className={' font-normal text-normal leading-9  mb-10'}>
                                Contact us directly by phone for quick assistance.
                            </Typography>
                        </div>

                        <Link href={'mailto:info@bizolutiontech.com'} className={'flex items-center gap-6  font-bold text-normal mb-6  '}>
                            <BiEnvelopeOpen size={24} className='text-content3' />   info@bizolutiontech.com
                        </Link>

                        <Link href={'tel:+918889501682'} className={'flex items-center gap-6  font-bold text-normal  mb-6  '}>
                            <BiPhoneCall size={24} className='text-content3' />    +91-8889501682
                        </Link>




                        <div className='flex gap-6 items-start  font-bold max-w-[395px] p-0 ' >
                            <BiMap size={64} className='text-content3' />
                            4/12, Second Floor, Shree Tower,
                            New Shanti Nagar, Raipur,
                            Chhattisgarh 492007
                        </div>
                    </div>


                </div>



                <div className='mx-[3rem]  w-[1003px] '>
                    <Typography variant='h1' className={'mt-16 mb-10 font-bold text-medium text-black-50  '}>
                        Contact Information
                    </Typography>
                    {/* FORM */}
                    <form
                        method="POST"
                        onSubmit={handleSubmit(onSubmit)}
                        className=" flex flex-col w-full">

                        <div>
                            <div className="flex flex-wrap mb-[1.111rem] gap-8 sm:flex-nowrap">

                                {/* First Name */}
                                <InputField
                                            type="text"
                                            placeholder="First name *"
                                            error={Boolean(errors.first_name)}
                                            errorMessage={errors?.first_name && "First Name is required"}
                                        />
                            </div>
                        </div>

                        <div >
                            <div className="flex flex-wrap mb-[1.111rem]  gap-8 sm:flex-nowrap">
                            <InputField
                                            type="text"
                                            className={'px-0 h-[3.556rem]'}
                                            placeholder="Contact no. *"
                                            error={Boolean(errors.mobile)}
                                            errorMessage={errors?.mobile?.type === 'required' ? "Phone Number is required" : (errors?.mobile?.type === 'pattern' || errors?.mobile?.type === 'maxLength' || errors?.mobile?.type === 'minLength') ? "Contact Number is invalid" : ''}
                                        />
                               

                            </div>
                        </div>

                        <Textarea
                                    minRows={20}
                                    placeholder="Write Message"
                                    className="col-span-12 md:col-span-6 mb-[2rem]   "
                                    classNames={{
                                        base: 'w-full  mx-0 px-0',
                                        input: [
                                            "text-primary-500 ",
                                            "text-normal",
                                            'outline-secondary-100',
                                            'outline-offset-0',
                                            "rounded-small",
                                            "mx-0",
                                            "w-full",
                                            "placeholder: pl-4",
                                        ],
                                        inputWrapper: [
                                            "bg-white",
                                            'outline-offset-0',
                                            "shadow-none",
                                            "max-w-1/2",
                                            "rounded-small",
                                            "px-0",
                                            "text-black",
                                            "!cursor-text",
                                        ],
                                    }}
                                    errorMessage={errors?.message && "Message is required"}
                                />

                        <div className='flex justify-end  '>

                            <Button type='submit' variant='solid' className='text-background lg:w-[248px] font-bold bg-primary-500 mb-8  '>
                                Send Message
                            </Button>
                        </div>




                    </form>
                </div>
            </div>












        </Card>
    )
}

