'use client'
import { Navbar as NextNavbar, NavbarBrand, NavbarContent, NavbarItem, NavbarMenuToggle, NavbarMenu, NavbarMenuItem, Image } from "@nextui-org/react";
import { useState } from "react";
import Link from "next/link";
import { MAX_WIDTH } from "@/constant";
import { Button } from "./Base";



export const Navbar = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [activeLink, setActiveLink] = useState('/')


  const menuItems = {
    links: [
      {
        title: 'Home',
        link: '/',
      },
      {
        title: 'About Us',
        link: '/about-us',
      },
      {
        title: 'Pricing',
        link: '/#pricing',
      },
      {
        title: 'Features',
        link: '/#features',
      },
      {
        title: 'Contact',
        link: '/#contact',
      },



    ]
  }

  // const pathname = usePathname()
  const getActive = (item: any) => {
    return activeLink === item.link
  }


  return (
    <NextNavbar
      isMenuOpen={isMenuOpen}
      onMenuOpenChange={setIsMenuOpen}
      isBlurred={false}
      classNames={{
        base: 'w-full z-50 bg-white shadow-md ',
        wrapper: `w-full max-w-[${MAX_WIDTH}px]  p-0 `,
        content: 'gap-6',
        item: [
          'text-normal',
          'font-medium',
          'text-center',
          "data-[active=true]:text-normal",
          // "data-[active=true]:px-[15px] py-1",
          // "data-[active=true]:rounded-[5px]",
          "data-[active=true]:text-content3",
          "data-[active=true]:font-bold",
        ],
      }}


    >
      <NavbarContent>
        <NavbarItem>

          <NavbarBrand className="">
            <Image alt={"logo"} className="rounded-none h-7" src="./assets/images/logo.svg" />
          </NavbarBrand>
        </NavbarItem>
      </NavbarContent>

      <NavbarContent className="hidden sm:flex " justify="center">
        {
          menuItems?.links?.map((item, index) => {
            return (
              <NavbarItem key={index} isActive={getActive(item)}  >
                <Link onClick={() => setActiveLink(item?.link)} className=" text-normal  " href={item.link}>
                  {item.title}
                </Link>
              </NavbarItem>
            )
          })
        }
      </NavbarContent>
      <NavbarContent justify="end">
        <NavbarItem className="sm:block">
          <Button as={Link} color="primary" href="https://entera.co.in/login" size="md" variant="solid">
            Login
          </Button>
        </NavbarItem>
        <NavbarItem className=" sm:block">
          <Button
            as={Link} color="primary" href="https://entera.co.in/sign-up" size="md" variant="bordered">
            Sign Up
          </Button>
        </NavbarItem>
        <NavbarMenuToggle
          aria-label={isMenuOpen ? "Close menu" : "Open menu"}
          className="sm:hidden text-primary"
        />
      </NavbarContent>
      <NavbarMenu

      >
        {menuItems?.links?.map((item, index) => (
          <NavbarMenuItem key={`${item}-${index}`}>
            <Link
              onClick={() => {
                setIsMenuOpen(!isMenuOpen)
                setActiveLink(item.link)
              }}
              className={`${activeLink === item.link ? "text-primary" : "text-secondary-50"} text-normal font-medium`}
              href={item?.link}
            >
              {item?.title}
            </Link>
          </NavbarMenuItem>
        ))}
      </NavbarMenu>
    </NextNavbar>
  );
}

