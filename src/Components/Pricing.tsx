'use client'

import React, { useEffect, useState } from 'react'
import { CenteredContainer, InputField, Typography, } from '@/Components/Base'
import { Radio, RadioGroup, Tab, Tabs, cn } from '@nextui-org/react'
import { PricingCard } from '../Components/Base/PricingCard'
import { BASE_URL } from '@/constant'
import pricing from "@/data/pricing.json"




export function Pricing() {
    const data = pricing
    // const [openExample, setOpenExample] = useState(false)
    // const [data, setData] = useState()
    const [planType, setPlanType] = useState('YEAR')


    let freePlan = data?.filter((i) => !i.is_paid)
    const [value, setValue] = useState(16)


    // useEffect(() => {
    //     fetch(`${BASE_URL}plan/plan-list`)
    //         .then((res) => res.json())
    //         .then((data) => {
    //             setData(data)
    //         })
    // }, [])


    return (
        <section id="pricing" className='bg-white  relative' >

            <div className='pt-10 bg-price-bg bg-cover h-[520px]  max-w-[1349px] mx-auto'>

                <Typography className='font-poppins text-center text-background text-large font-medium mb-[1.111rem] min-w-[320px]' variant='h1'>
                    Our
                    <span className="text-content3 font-poppins font-medium"> Subscription Plan</span></Typography>

                {/* input section */}
                <div className="w-full mx-auto text-center py-[21px]">
                    <div className='flex justify-center items-center '>

                        <Typography variant='h1' className={'text-medium text-background'}>
                            How many employees are in your company ?
                        </Typography>

                        <InputField
                            type="number"
                            value={value}
                            onChange={(e: any) => setValue(e.target.value)}
                            className={'px-0  pl-5'}
                            // isClearable
                            // radius="lg"
                            classNames={{
                                base: 'max-w-[6.778rem]',
                                input: [
                                    "bg-primary",
                                    "text-primary-500 ",
                                    "text-medium",
                                    // 'outline-primary',
                                    // "rounded-medium",
                                    // "mx-0",
                                    "h-[2.222rem]",
                                    // // "pl-2",
                                    "text-center",
                                    "rounded-2xl",
                                ],
                                // innerWrapper: "rounded-none",
                                inputWrapper: [
                                    "shadow-none",
                                    "h-[2.222rem]",
                                    "max-w-[6.778rem]",
                                    "rounded-2xl",
                                    'outline-primary'
                                ],
                            }}
                        />

                    </div>

                    <div className="flex place-content-center flex-wrap gap-6 mt-4">
                        <Typography variant={'text-medium'} className='text-center font-normal text-background'>
                            How often would you like to be billed?
                        </Typography>
                        <RadioGroup
                            orientation="horizontal"
                        >
                            <Radio  
                                  classNames={{
                                    label: cn(
                                      " text-background ",
                                  
                                    ),
                                  }}
                            
                            
                             value="monthly">Monthly</Radio>
                            <Radio  
                                  classNames={{
                                    label: cn(
                                      " text-background ",
                                  
                                    ),
                                  }}
                            
                            
                             value="annual">Annually</Radio>
                        </RadioGroup>
                    </div>


                    <Typography variant='text-paragraph' className={'text-background font-bold mt-3 cursor-pointer'}>
                        See Pricing Example
                    </Typography>
                </div>

             
            </div>
            {/* Heading */}

            <CenteredContainer className='flex gap-6 flex-wrap-reverse items-center  justify-center -mt-[20rem] lg:-mt-[10.444rem]'>


                {
                    data?.map((item, index) => {
                        return (
                            <div key={index}>
                                {
                                    item?.is_paid ?
                                        <PricingCard
                                            discount={item.plan_time_frequency_type === 'YEAR' ? 25 : 0}
                                            value={value}
                                            planType={planType}
                                            data={item}
                                            minimum_value={freePlan[0]?.maximum_employee} />
                                        :
                                        <PricingCard
                                            value={value}
                                            planType={planType}
                                            data={item}
                                            minimum_value={freePlan[0]?.maximum_employee} />
                                }
                            </div>

                        )
                    })
                }

            </CenteredContainer>

            {/* <Modal size={'4xl'} isOpen={openExample} onClose={() => setOpenExample(!openExample)}>
                <PricingDetail data={data?.data} value={value} />
            </Modal> */}
        </section>
    )
}
