import React from 'react'
import { Button, Typography } from './Base'
import {  IoIosPodium,IoIosRocket ,IoIosAddCircle} from 'react-icons/io';
// react-icons/io/IoIosRocket react-icons/io/IoIosAddCircle


export const Header = () => {
    return (
        <section className='bg-hero-banner h-[400px] sm:h-[832.31px] bg-cover mb-[150px]'>

            <div className='max-w-[685px] pt-[120px] text-center sm:text-left sm:ml-60'>
                <Typography variant={'header'} >
                    CRM Software <br />
                    for  <span className="text-primary">Growing Startups
                    </span>
                </Typography>
                <Typography variant={'text-medium'} className='leading-9 mb-10' >
                    Empower your startup for growth by implementing our CRM software, fine-tuned to streamline operations and boost customer engagement.

                </Typography>

                <Button href='/#pricing' variant='solid' size='xl' className='bg-primary text-background   font-bold mb-20'>
                Let’s Start
                </Button>
            <div className="  items-center gap-20 hidden lg:flex">
                <div className="flex items-center gap-2 w-[160px]">
                    <IoIosRocket size={64 } color="#BA6CFF"/>
                    <Typography variant={'text-paragraph'} className='font-bold leading-6'>
                    Quick  Setup
                        </Typography> 
                </div>
                <div className="flex items-center gap-2 w-[160px]">
                    <IoIosAddCircle size={64} color="#BA6CFF"/>
                    <Typography variant={'text-paragraph'} className='font-bold leading-6'>
                    Add <br/> Leads
                        </Typography> 
                </div>
                <div className="flex items-center gap-2 w-[160px]">
                    <IoIosPodium size={64} color="#BA6CFF"/>
                    <Typography variant={'text-paragraph'} className='font-bold leading-6'>
                    Get <br/> Deals
                        </Typography> 
                </div>
            </div>
            </div>





        </section>
    )
}

