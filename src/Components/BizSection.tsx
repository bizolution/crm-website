"use client"
import { Card } from '@nextui-org/react'
import { Image } from '@nextui-org/react'
import React from 'react'
import { Button, Typography } from './Base'

const BizSection = () => {
  return (
    <Card className='w-[1320px] flex flex-col justify-center gap-5 items-center bg-[#FCFFFBCC] py-7  rounded-none'>
        <Image alt='adasd' className='rounded-none  p-0' src="/assets/images/biz_logo.png" />

        <Typography variant="text-medium" className='text-center p-0'>
        Bizolution Technologies is the parent company of Entera CRM product which is the new face of tech solutions in corporate world, 
        <br/>
        as advanced services are the answers to a successful enterprise, whether new or old.
        </Typography>

        <Button href='/' variant='solid' size='xl' className='bg-primary rounded-full font-bold text-normal text-background'>
            Know More
        </Button>
    </Card>
  )
}

export default BizSection