'use client'

import React from 'react'
import { Accordion, AccordionItem } from "@nextui-org/react";
import { AiFillPlusCircle } from 'react-icons/ai';
import { AiFillMinusCircle } from 'react-icons/ai';

import faq from "@/data/faq.json"
import { CenteredContainer, Typography } from '@/Components/Base';



export function FAQ() {
    const data = faq
    const itemClasses = {
        base: "mb-[14px] text-paragraph group-[.is-splitted]:shadow-md group-[.is-splitted]:bg-white  group-[.is-splitted]:rounded-none group-[.is-splitted]:px-[3.056rem] group-[.is-splitted]:py-[1.222rem]",
        trigger: "rounded-none py-0  mb-0",
        // heading:'text-paragraph',
        indicator: "text-medium data-[open=true]:rotate-0",
        content: "text-normal",
        // base:'bg-primary'
    };


    return (
        <div id="faq" >

            <CenteredContainer className='bg-white mt-[150px] mx-10'>
                <div >
                    <Typography variant={'heading'} >
                        Frequently Ask <span className='text-primary-500'> Questions </span> (FAQ)

                    </Typography>
                    <Typography variant={'paragraph'} >
                        Discover what they have to say and experience the difference for your startup.

                    </Typography>

                    <Accordion
                        itemClasses={itemClasses} variant="splitted"
                        defaultExpandedKeys={'1'}
                    >
                        {
                            data?.map((item) => {
                                return (
                                    <AccordionItem
                                        indicator={({ isOpen }) => (!isOpen ? <AiFillPlusCircle className='bg-content3 text-white  rounded-full border' /> : <AiFillMinusCircle className='bg-content3 text-white  rounded-full border' />)}
                                        className='text-normal text-secondary-50 font-medium border' key={item.id} aria-label="Accordion 1" title={
                                            <Typography className={'text-paragraph font-bold'}>
                                                {item.ques}
                                            </Typography>
                                        }>
                                        {item.ans}
                                    </AccordionItem>
                                )
                            })}

                    </Accordion>
                </div>
            </CenteredContainer>
        </div>

    )
}

