'use client'
import React from 'react'
import feature from '@/data/feature.json'
import { Typography } from '@/Components/Base'
import { Card, CardHeader } from '@nextui-org/react'
import { IoMdThumbsUp, IoMdKey, IoMdConstruct, IoMdStats } from 'react-icons/io';

export const Features = () => {
    const data = feature
    return (
        <section className='mb-[150px]' id="features" >
            <Typography variant={'heading'} >

                Optimized    <span className='text-primary'>
                    CRM Solution     </span>

                for Your Budding Startup

            </Typography>
            <Typography variant={'paragraph'} >
                {`Streamline Your Startup's Operations with Our Optimized CRM Solution, Enhancing Efficiency and Customer Engagement for Your Growing Business.`}
            </Typography>
            <div className="flex gap-5 flex-wrap justify-center">

                {/* Feature One */}
                <Card className={`h-[400px] w-[323px] bg-cover bg-[url('/assets/images/Vector.png')] justify-center items-center`}>
                    <CardHeader className=" p-4 z-10 my-12 w-[264px]  h-[300px] flex flex-col  justify-center items-start bg-content2 rounded-lg ">
                        <IoMdThumbsUp size={64} color="#5E46C9" />
                        <h1 className='text-content1 leading-none my-2 font-bold text-[24px] '>
                            Easy to setup
                        </h1>
                        <h2 className='text-secondary-50 font-medium  w-[226px] text-[18px]'>
                            Hassle-free setup, along with an intuitive interface and guided onboarding, makes it easy for users of all levels to get up and running smoothly.
                        </h2>
                    </CardHeader>
                </Card>

                {/* Feature TWo */}
                <Card className={`h-[400px] w-[323px] bg-cover bg-[url('/assets/images/card-2.png')]  justify-center items-center`}>
                    <CardHeader className=" p-4 z-10 my-12 w-[264px] h-[300px] flex flex-col  justify-center items-start   bg-content2 rounded-lg ">
                        <IoMdKey size={64} color="#5E46C9" />
                        <h1 className='text-content1 leading-none my-2 font-bold text-[24px] '>
                            Access details in few clicks
                        </h1>
                        <h2 className='text-secondary-50 font-medium  w-[226px] text-[18px]'>
                            Effortlessly retrieve crucial, up-to-date information with just a few clicks, streamlining your workflow.
                        </h2>
                    </CardHeader>
                </Card>

                {/* Feature Three */}
                <Card className={`h-[400px] w-[323px] bg-cover  bg-[url('/assets/images/card3.png')] justify-center items-center`}>
                    <CardHeader className=" p-4 z-10 my-12 w-[264px] h-[300px] flex flex-col  justify-center items-start   bg-content2 rounded-lg ">
                        <IoMdConstruct size={64} color="#5E46C9" />
                        <h1 className='text-content1 leading-none my-2 font-bold text-[24px] '>
                            Setup you own pipeline
                        </h1>
                        <h2 className='text-secondary-50 text-[18px] font-medium w-[226px]'>
                            Empower your business with the flexibility to design and set up your own unique pipeline effortlessly.
                        </h2>
                    </CardHeader>
                </Card>

                {/* Feature Four */}
                <Card className={`h-[400px] w-[323px] bg-cover bg-[url('/assets/images/card4.png')] justify-center items-center`}>
                    <CardHeader className=" p-4 z-10 my-12 w-[264px] h-[300px]  flex flex-col  justify-center items-start   bg-content2 rounded-lg ">
                        <IoMdStats size={64} color="#5E46C9" />
                        <h1 className='text-content1 leading-none my-2 font-bold text-[24px] '>
                            Data Insights
                        </h1>
                        <h2 className='text-secondary-50 text-[18px] font-medium w-[226px]'>
                            Maximize the potential of your CRM data with our comprehensive analysis tools, providing valuable insights for strategic decision-making.
                        </h2>
                    </CardHeader>
                </Card>

            </div>
        </section>
    )
}
