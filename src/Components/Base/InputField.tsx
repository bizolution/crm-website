'use client'
import { Input } from '@nextui-org/react'
import React from 'react'



export const InputField = ({ ...rest}) => {
  return   <Input
  className={'px-0 h-[3.556rem]'}
  classNames={{
    base: 'max-w-1/3  mx-0 px-0 h-[3.556rem] ',
    input: [
      "text-primary-500",
      "text-normal",
      "border-1",
    //   'outline-secondary-100',
    //   'outline-offset-[2px]',
      "rounded-small",
      "mx-0",
      "max-w-1/2",
      "placeholder:text-secondary-50 pl-4",
    ],
    inputWrapper: [
      "hover:bg-white",
      "bg-white",
      "outline-none",
      "shadow-none",
      "max-w-1/2",
      "border-1",
      "h-[64px]",
      "rounded-small",
      "px-0",
      "text-black",
      "!cursor-text",
    ],
  }}
  {...rest}
 />
}

