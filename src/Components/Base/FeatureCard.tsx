'use client'
import React, { ReactNode } from 'react'
import { Card, CardFooter, Image, Button, CardHeader, CardBody } from "@nextui-org/react";
import { Typography } from '@/Components/Base/Typography';


interface CardTypes {
  title?: String,
  subtitle?: String,
  icon?: string,
  bgImg: string

}


export const FeatureCard = ({ title, subtitle, icon, bgImg }: CardTypes) => {
  return (

    <Card className={`h-[400px] w-[323px] bg-cover  justify-center items-center`}>
      <CardHeader className=" p-4 z-10 my-12 w-[264px]  flex flex-col  justify-center items-start  bg-opacity-80 bg-content2 rounded-lg ">
        <Image alt={'Feature'} height={'64px'} width={'64px'} className='text-primary rounded-none' src={icon} />
        <Typography variant={'text-paragraph'} className='text-content1 leading-none my-4 font-bold '>
          {title}
        </Typography>
        <Typography className='text-secondary-50 leading-6 w-[226px]'>
          {subtitle}
        </Typography>
      </CardHeader>
    </Card>


  )
}









