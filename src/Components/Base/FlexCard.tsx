'use client'

import React from 'react'
import { Typography } from './Typography'
import { Image } from "@nextui-org/react";

interface flexCardTypes {
  reverse: Boolean,
  item: any
}

export const FlexCard = ({ reverse, item }: flexCardTypes) => {
  return (
    <div className={`flex flex-wrap ${reverse ? 'flex-row-reverse' : 'flex-row'} justify-center items-center mb-[5.556rem] sm:gap-28`}>
      <Image alt={item?.image} className={'max-h-[22.056rem]'} src={item?.image} />

      <div className=' h-[22.056rem] flex flex-col justify-center max-w-[32.111rem]'>
        <Typography variant='h1' className='text-large font-bold mb-[1.667rem] font-gothic capitalize ' >

          <span className="text-primary-500">Better </span>
          {item.title}
        </Typography>
        <Typography variant='h1' className={'text-medium font-normal leading-8 text-secondary-50'}>
          {item.desc}
        </Typography>

      </div>
    </div>
  )
}
