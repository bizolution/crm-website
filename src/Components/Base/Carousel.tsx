'use client'
// Default theme
import '@splidejs/react-splide/css';

// or other themes
import '@splidejs/react-splide/css/skyblue';
import '@splidejs/react-splide/css/sea-green';

// or only core styles
import '@splidejs/react-splide/css/core';
import React, { useEffect, useState } from 'react'

import { Splide, SplideSlide } from '@splidejs/react-splide';
import '@splidejs/react-splide/css';







interface CardType {
    children: React.ReactNode
}

export const CardCarousel = ({ children, ...rest }: CardType) => {
    const [screenSize, setScreenSize] = useState(0)
    useEffect(() => {
        window.addEventListener('resize', () => {
            setScreenSize(window.outerWidth)
        })
    }, [screenSize])


    return (

        <Splide
            className='max-w-[1320px] mx-auto mb-[150px] bg-white shadow-none'

            options={{
                rewind: true,
                perPage: screenSize >= 600 ? 3 : 1,
                gap: screenSize >= 600 ? '75px' : '52px'
            }}
            // onAutoplayPlay={() => (true)
            {...rest}
        >
            {children}
        </Splide>


    )
}



export const CarouselItem = ({ children, ...rest }: CardType) => {
    return (
        <SplideSlide className="shadow-lg"  {...rest} >
            {children}
        </SplideSlide>
    )
}



