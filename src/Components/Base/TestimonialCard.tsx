'use client'
import React, { ReactNode } from 'react'
import { Card, Image, CardHeader, CardBody } from "@nextui-org/react";
import { Typography } from '@/Components/Base/Typography';
import sample from "@/assets/images/sample.svg"




interface CardTypes {
    heading?: String,
    footer?: ReactNode

}


export const TestimonialCard = ({ heading, footer }: CardTypes) => {
    return (
        <Card className='bg-white '>
            <CardHeader className="p-0 ">
                <Image
                    removeWrapper
                    alt="img"
                    className="z-0  h-full object-cover rounded-none"
                    src='./assets/images/demo.png'

                    // src='https://imgs.search.brave.com/09HSwZu4YqIqYDx-JZSCq_3Gi2NvHbUHmKgcHkSA9Iw/rs:fit:860:0:0/g:ce/aHR0cHM6Ly93YWxs/cGFwZXJjYXZlLmNv/bS93cC93cDM1NDQx/MDMuanBn'
                    width={'400px'}
                />
            </CardHeader>
            <CardBody className='py-5 px-9'>
                <Typography variant={'text-normal'} className='text-secondary-50 mb-5 leading-6 font-normal max-w-[400px]'>
                    <span className='text-large  align-top text-primary pr-2 font-bold'>“</span>
                    {`I'm delighted with the performance of the Entera payroll we've been using. Attendance tracking, leave management payroll generation`}
                    <span className="text-content3 cursor-pointer">
                        ...Read More
                    </span>
                </Typography>

                <Typography variant={'text-paragraph'} className='font-bold  text-primary'>
                    Shweta Sachdev
                </Typography>
                <Typography variant={'text-small'} className='text-secondary-50'>
                    Product Manager, Entera
                </Typography>
            </CardBody>
        </Card>
    )
}









