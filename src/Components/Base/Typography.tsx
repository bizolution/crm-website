interface Types {
  children:String | any,
  className?:string,
  variant?:String
}


export const Typography = ({ variant = 'text-normal', children,className, ...rest }:Types) => {
  switch (variant) {
    case 'text-large':
      return <h1 className={className + ' text-large'} {...rest}>{children}</h1>;
    case 'text-medium':
      return <h1 className={className + ' text-medium'} {...rest}>{children}</h1>;
    case 'text-normal':
      return <h1 className={className + ' text-normal'} {...rest}>{children}</h1>;
    case 'text-small':
      return <h1 className={className + ' text-small'} {...rest}>{children}</h1>;
    case 'text-paragraph':
      return <p className={className + ' text-paragraph'} {...rest}>{children}</p>;
    case 'header':
      return <p className={className + ' max-w-[685px] font-poppins text-large font-medium lg:leading-[60px] mb-4'} {...rest}>{children}</p>;
    case 'heading':
       return <h1 className={className + ' font-poppins text-large font-medium text-center mb-2'} {...rest}>{children}</h1>;
    case 'paragraph':
       return <h1 className={className + ' max-w-[855px] text-center text-medium mx-auto text-secondary-50 leading-9 mb-20'} {...rest}>{children}</h1>;
    default:
      return <p className={className} {...rest}>{children}</p>;
  }
};

