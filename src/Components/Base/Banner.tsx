import React from 'react'

interface BannerTypes{
    children: React.ReactNode
}

export const   Banner = ({ children }:BannerTypes) => {
  return (
    <header className="h-[20.944rem] overflow-x-clip mb-[8.333rem] bg-about-banner bg-cover bg-no-repeat flex justify-center items-center w-full">
      {children}
    </header>
  )
}

