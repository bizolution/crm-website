'use client'
import {extendVariants} from "@nextui-org/react";
import { Button as UIButton } from '@nextui-org/button'

export const Button = extendVariants(UIButton, {
  variants: {
    // <- modify/add variants
    // color: {
    //   olive: "text-[#000] bg-[#84cc16]",
    //   orange: "bg-[#ff8c00] text-[#fff]",
    //   violet: "bg-[#8b5cf6] text-[#fff]",
    // },
    isDisabled: {
      true: "bg-[#eaeaea] text-[#000] opacity-50 cursor-not-allowed",
    },
    size: {
      xs: "px-unit-2 min-w-unit-12 h-unit-6 text-tiny gap-unit-1 rounded-large",
      md: "px-unit-4 min-w-unit-20 h-unit-10 text-small gap-unit-2 rounded-medium border-1",
      xl: "px-5 py-2.5 h-auto text-medium rounded-medium",
      normal: "px-[1.667rem] py-[0.667rem] h-auto text-normal rounded-large text-white",

    },
  },
  compoundVariants: [ // <- modify/add compound variants
    {
      isDisabled: true,
      color: "primary",
      class: "bg-[#84cc16]/80 opacity-100",
    },
  ],
});