import { MAX_WIDTH } from '@/constant'
import React from 'react'

interface ContainerTypes {
    children: React.ReactNode,
    className?:string
}

export const CenteredContainer = ({children , className}:ContainerTypes) => {
  return (
   <section className={`max-w-[${MAX_WIDTH}px] mx-auto ` + className}>
    {children}
   </section>
  )
}

