'use client'

import { Card } from '@nextui-org/react'
import React from 'react'


import { BiCheck } from 'react-icons/bi';

import Link from 'next/link';
import { SITE_URL } from '@/constant';
import { numberWithCommas } from '@/utils';
import { Button, Typography } from '.';



const PricingCard = ({ data, value, minimum_value, discount }: any) => {
    // let isStartup = true
    let isSelected = data?.is_paid ? value > Number(minimum_value) : value <= Number(minimum_value)



    return (
        <Card className={`cursor-pointer bg-background  rounded-small drop-shadow-md p-9 w-[27.111rem] text-center}`}>
            <div>
                <Typography
                    variant='h1' className={'text-black-50 text-large font-bold'}>
                    {data?.title}
                </Typography>

                <Typography variant='p' className={'mt-3 text-primary-500 text-medium font-normal'}>
                    {data?.description}
                </Typography>

                <div className="border-t-1 border-b-1  my-2 border-[#BA6CFF]">

                    <div className='flex items-center gap-2 '>

                    <Typography className={'text-large '}>
                        {discount ?
                            <span className='font-normal text-secondary-50 text-medium line-through mr-2 '>₹ {Number(data?.original_price)}</span> : null}
                        <span className='text-large'>₹</span>
                        <span className="ml-2 text-primary-500 text-large font-bold font-gothic">{Number(data?.price)}</span>
                    </Typography>
                    <Typography variant='p' className={'mt-3 font-normal text-secondary-50      text-small'}>
                    {data?.is_paid ? 'Per User / Month billed annually' : 'Always Free'}
                </Typography>
                    </div>

                   
                <Typography variant='p' className={'text-content3 font-bold my-3'}>
                  
                    {data.is_paid ? ` ${numberWithCommas(Number(data?.price * value * 12))} for ${value} Users` : ' Get full feature access'}
                </Typography>
                </div>


              
            </div>

      

            {/* <div className="mt-5">
                {
                    data?.MasterPlanDetail?.map((item: any) => {
                        return (
                            <div className='flex items-center gap-3 mb-3' key={item.id}>
                                <BiCheck className='text-primary-500' size={'24px'} />
                                <Typography variant='h1' >
                                    {item.title}
                                </Typography>
                            </div>
                        )
                    })
                }
            </div> */}

            <Button as={Link} variant={isSelected ? 'solid' : 'bordered'} 
            href="/#pricing"
            // href={`${SITE_URL}sign-up/${data?.id}`}
            fullWidth            
            className={isSelected ? 
                'font-bold py-[12px]  mt-3 bg-primary-500 text-background text-medium rounded-3xl' : 
                'rounded-3xl  py-[12px]  text-white-50 text-medium font-bold  mt-3 border-primary'} 
            >
            Start Now 
            </Button>
        </Card>
    )
}

export { PricingCard }