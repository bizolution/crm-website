'use client'
import { Image } from '@nextui-org/react'
import React from 'react'

import { IoLogoLinkedin, IoLogoFacebook } from 'react-icons/io';
import Link from 'next/link';
import { CenteredContainer } from './Base';
import { Contact } from '.';



export const Footer = ({ isAbout }: any) => {

  return (
    <footer className={`${isAbout ? 'mt-0' : ' mt-[355px] '} + bg-footer-texture text-background  px-6 lg:px-0 `}>
      {/* <CenteredContainer className='flex flex-col justify-end h-[481px] relative'>    ${!isAbout ? 'h-[481px]' : 'h-full'} */}
      <CenteredContainer className={`flex flex-col justify-end  relative `}>

        {
          isAbout ? null :
            <div className=" w-full">
              <Contact />
            </div>
        }

        <nav className='hidden sm:flex  sm:justify-between my-14  flex-wrap'>

          <Image alt={"Image"} className='rounded-none h-9' src="./assets/images/footer_logo.svg" />



          <div className='flex flex-col'>
            <h3 className='mb-4 text-normal font-medium'>Company</h3>
            <Link href="/about-us" className='text-small cursor-pointer mb-1'> About us</Link>
            <Link href={'/#features'} className='text-small cursor-pointer mb-1'>Features</Link>
            <Link href={'/#faq'} className='text-small cursor-pointer mb-1'>FAQ</Link>
          </div>

          <div className='flex flex-col'>
            <h3 className='mb-4 text-normal font-medium'>Contact Us</h3>
            <Link href={'mailto:info@bizolutiontech.com'} className='text-small mb-1'> info@bizolutiontech.com</Link>
            <Link href={'mailto:info@bizolutiontech.com'} className='text-small mb-1'> hr@bizolutiontech.com</Link>

            <p className='text-small mb-1'>+91-9993896800</p>
            <p className='text-small mb-1'>+918889501682</p>
          </div>

          <div className='sm:block'>
            <h3 className='mb-4 text-normal font-medium'>Social</h3>
            <div className="flex gap-4">
              <IoLogoLinkedin size={24} />
              <IoLogoFacebook size={24} />
            </div>
          </div>
        </nav>


        <nav className='sm:hidden mb-[3.333rem] flex-wrap'>

          <Image alt={'Logo'} className='rounded-none h-9 px-14' src="./assets/images/logo.svg" />
          <section className="flex justify-between py-9 px-14">
            <div>
              <h3 className='mb-4 text-normal font-medium'>Company</h3>
              <p className='text-small'> About us</p>
              <p className='text-small'>Privacy Policy</p>
              <p className='text-small'>Careers</p>
            </div>
            <div className='sm:block'>
              <h3 className='mb-4 text-normal font-medium '>Social</h3>
              <div className="flex">
                <IoLogoLinkedin size={24} />
                <IoLogoFacebook size={24} />
              </div>


            </div>
          </section>



          <div className='py-9 px-14'>
            <h3 className='mb-4 text-normal font-medium'>Contact Us</h3>
            <p className='text-small'> hr@bizolutiontech.com</p>
            <p className='text-small'>+91-9993896800</p>
          </div>


        </nav>
        <hr />

        <h2 className='text-center my-[1.722rem] text-normal font-medium '>© 2023 Bizolution Technologies | All Rights Reserved</h2>
      </CenteredContainer>
    </footer>
  )
}
