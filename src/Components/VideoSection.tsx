'use client'
import React from 'react'
import { CenteredContainer, Typography } from './Base'
import { Image } from '@nextui-org/react'

export const VideoSection = () => {
    return (
        <div className='bg-video-bg h-[600px] bg-cover mb-[372px] '>
            <CenteredContainer className='p-10 gap-2 flex flex-col  items-center'>
                <Typography variant='text-large' className='font-poppins text-center mb-20 text-background lg:max-w-[650px] mx-auto leading-relaxed'>
                    <span className='text-[#BA6CFF]'>CRM Features
                    </span>   for
                    Enhanced Business Performance
                </Typography>

                <video autoPlay={true}  src={"/assets/images/demo_video.mp4"} className='rounded-2xl h-[581px] w-[1033px] shadow-2xl    ' />

                {/* <Image alt='adasd' className='rounded-2xl h-[581px] w-[1033px] shadow-lg  ' src="./assets/images/contact-bg.png" /> */}
            </CenteredContainer>

        </div>
    )
}

