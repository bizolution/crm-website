'use client'
import React from 'react'
import feature from '@/data/feature.json'
import { CenteredContainer, TestimonialCard, CarouselItem, CardCarousel, Typography } from '@/Components/Base'
// import { CardCarousel, CarouselItem } from './Base/Carousel'

export const Testimonial = () => {
    const data = feature
    return (
        <CenteredContainer  >
            <Typography variant={'heading'} >
                Hear What  <span className="text-primary">    Our trusted Clients</span>      Have to Say
            </Typography>
            <Typography variant={'paragraph'} >
                {`Our customers' testimonials are a testament to the quality of our products and services, highlighting the real impact we've had on their businesses and lives.`}
            </Typography>
            {/* <div className="flex"> */}
            <CardCarousel>
                <CarouselItem>
                    <TestimonialCard
                    // heading={'askjdhakjdh'}
                    />
                </CarouselItem>
                <CarouselItem>
                    <TestimonialCard
                    // heading={'askjdhakjdh'}
                    />
                </CarouselItem>
                <CarouselItem>
                    <TestimonialCard
                    // heading={'askjdhakjdh'}
                    />
                </CarouselItem>
                <CarouselItem>
                    <TestimonialCard
                    // heading={'askjdhakjdh'}
                    />
                </CarouselItem>
            </CardCarousel>
            {/* </div> */}
        </CenteredContainer>
    )
}
