import { FAQ, Features, Pricing, Testimonial,Header,VideoSection } from '@/Components'
import { Footer } from '@/Components/Footer'






export default function Home() {

  return (
    <main>
      <Header/>
      <Features />
      <VideoSection/>
      <Testimonial />
      <Pricing />
      <FAQ />
      <Footer />
    </main>
  )
}
