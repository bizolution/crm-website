import './globals.css'
import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import { Providers } from './providers'
import { Navbar } from '@/Components'


const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'Entera CRM',
  description: 'Entera CRM',
}

export default function RootLayout({children}:{children: React.ReactNode}) {
  return (
    <html lang="en">
      <body className={inter.className + ' bg-white'}>
        <Providers>
         <Navbar/>
          {children}
    
        </Providers>
      </body>
    </html>
  )
}
