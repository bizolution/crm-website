'use client'

import { CenteredContainer, Typography } from "@/Components/Base";
import { Image } from "@nextui-org/react";
import { Banner } from "@/Components/Base/Banner";
import BizSection from "@/Components/BizSection";
import { Footer } from "@/Components/Footer";
import about from "@/data/about.json";


export default function AboutUs() {
    const data = about
    return (
        <main>
            <Banner>
                <div className='  mb-[4.444rem] text-center'>

                    {/* HEADING */}
                    <Typography variant='heading' className="text-background" >
                        What do we  <span className='text-[#BA6CFF] font-medium'>offer ? </span>
                    </Typography>

                    {/* PARAGRAPH */}
                    <Typography variant='p' className="max-w-[570px] text-background leading-8 font-normal">
                        We Help You Achieve Your Business Goals! <br />
                        Here are the business benefits of custom software solutions
                    </Typography>
                </div>

            </Banner>
            <CenteredContainer className={'mb-[4.444rem]'}>
                <section className="flex flex-row justify-center flex-wrap">

                    {/* About One */}
                    <div className={`flex flex-wrap ${'flex-row'} justify-center items-center mb-[5.556rem] sm:gap-28`}>
                        <Image alt={'About Us'} className={'max-h-[22.056rem]'} src={'./assets/images/aboutOne.svg'} />

                        <div className=' h-[22.056rem] flex flex-col justify-center max-w-[32.111rem]'>
                            <Typography variant='h1' className='text-large font-medium mb-[1.667rem] max-w-[571px] font-poppins capitalize ' >

                                <span className="text-primary-500">Better </span>
                                Business Reporting
                            </Typography>
                            <Typography variant='h1' className={'text-medium font-normal leading-8 text-secondary-50'}>
                                To enhance reporting capabilities and access real-time information, the implementation of a single integrated database encompassing all business processes is crucial.
                            </Typography>

                        </div>
                    </div>
                    {/* About Two */}
                    <div className={`flex flex-wrap ${'flex-row-reverse'} justify-center items-center mb-[5.556rem] sm:gap-28`}>
                        <Image alt={'About Us'} className={'max-h-[22.056rem]'} src={'./assets/images/aboutTwo.svg'} />

                        <div className=' h-[22.056rem] flex flex-col justify-center max-w-[32.111rem]'>
                            <Typography variant='h1' className='text-large font-medium mb-[1.667rem] font-poppins capitalize max-w-[571px] ' >

                                <span className="text-primary-500">Enhanced </span>
                                Customer Service
                            </Typography>
                            <Typography variant='h1' className={'text-medium font-normal leading-8 text-secondary-50'}>
                                By enhancing access to customer information, businesses can achieve faster response times, improved on-time delivery, and enhanced order accuracy, ultimately boosting customer satisfaction and operational efficiency.
                            </Typography>

                        </div>
                    </div>


                    {/* About Three */}
                    <div className={`flex flex-wrap ${'flex-row'} justify-center items-center mb-[5.556rem] sm:gap-28`}>
                        <Image alt={'About Us'} className={'max-h-[22.056rem]'} src={'./assets/images/aboutThree.svg'} />

                        <div className=' h-[22.056rem] flex flex-col justify-center max-w-[32.111rem]'>
                            <Typography variant='h1' className='text-large font-medium mb-[1.667rem] font-poppins capitalize max-w-[571px] ' >

                                Cost
                                <span className="text-primary-500"> Savings </span>
                            </Typography>
                            <Typography variant='h1' className={'text-medium font-normal leading-8 text-secondary-50'}>
                                Through improved inventory planning, businesses can provide better customer service, leading to strengthened and more fruitful business relationships.
                            </Typography>

                        </div>
                    </div>
                    {/* About Four */}
                    <div className={`flex flex-wrap ${'flex-row-reverse'} justify-center items-center mb-[5.556rem] sm:gap-28`}>
                        <Image alt={'About Us'} className={'max-h-[22.056rem]'} src={'./assets/images/aboutFour.svg'} />

                        <div className=' h-[22.056rem] flex flex-col justify-center max-w-[32.111rem]'>
                            <Typography variant='h1' className='text-large font-medium mb-[1.667rem] font-poppins capitalize max-w-[571px] font-poppins ' >


                                Business Process   <span className="text-primary-500">Improvements </span>
                            </Typography>
                            <Typography variant='h1' className={'text-medium font-normal leading-8 text-secondary-50'}>
                                By automating manual or routine tasks and implementing smarter workflows, businesses can gain efficiency and streamline their operations effectively.
                            </Typography>

                        </div>
                    </div>

                    {/* {
                        data?.map((item, index) => {
                            return <FlexCard reverse={item.id % 2 == 0} item={item} />
                        })
                    } */}
                </section>

                <BizSection />
            </CenteredContainer>
            <Footer isAbout={true} />
        </main>

    )
} 